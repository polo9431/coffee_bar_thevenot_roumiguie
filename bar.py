import random
import csv

PRICE_MAX = 10
HISPTER_PROB = 33
TRIP_PROB = 10
ONETIME_PROB = 80
random.seed(a=None)
NB_CUSTOMER = 1000
PROB_FILE = "tt.csv"

class barProb():
    def __init__(self, lst):
        self.time = lst[0]
        self.prob = {}
        for a in range(1, len(lst) - 1):
            tmp = lst[a].split(" ")
            self.prob[tmp[0]] = int(tmp[1])

    def getHour(self):
        return (self.time)

    def getProb(self, food):
        return (self.prob[food])

class customer():
    def __init__(self, _type, sold, _id):
        self.id = _id
        self.sold = sold
        self.hist = []
        self.type = _type

    def getStory(self):
        for a in self.hist:
            yield a

    def canBuy(self, price):
        if (self.sold < price):
            return (False)
        return (True)

    def buy(self, product, price):
        self.sold -= price
        self.hist.append("buy {} : for {}".format(product, price))

    def getType(self):
        return (self.type)

    def getId(self):
        return (self.id)

class bar():
    def __init__(self):
        self.seed = random.randint(100000, 900000)
        self.food = ["sandwich", "pie", "muffin", "cookie", "nothing"]
        self.drink = [ "milkshake", "frappucino" , "soda" , "coffee" , "tea" , "water"]
        self.proba = self.getProb()
        self.daylen = len(self.proba)
        self.price = {"sandwich" : 5, "cookie" : 2, "milkshake" : 5, "frappucino" : 4, "water" : 2, "def" : 3}
        self.client = [ self.genClient("return") for x in range(NB_CUSTOMER)]
        self.saveclient = self.client[:]

    def getProb(self):
        prob = []
        with open(PROB_FILE, 'r') as csvfile:
            spamreader = csv.reader(csvfile, delimiter=';')
            for a in spamreader:
                prob.append(barProb(a))
        return (prob)

    def genClient(self, _type):
        self.seed += 1
        if (_type == "return"):
            if (random.randint(1, 100) < HISPTER_PROB):
                return (customer("hipster", 500, self.seed))
            return (customer("return", 250, self.seed))
        else:
            if (random.randint(1, 100) < TRIP_PROB):
                return (customer("trip", 100, self.seed))
            return (customer("oneTime", 100, self.seed))

    def probCorespond(self, rand, hour, lst):
        for fd in lst:
            if (rand <= self.proba[hour].getProb(fd)):
                if (fd == "nothing"):
                    return ('')
                return (fd)

    def genYears(self, start, years, _file):
        with open(_file, 'w') as fd:
            fd.write("TIME;CUSTOMER;DRINKS;FOOD;\n")
            lst = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
            for a in range(years):
                y = 2000 + start + a
                if ((y % 4 == 0 and y % 100 != 0) or y % 400 == 0):
                    lst[1] = 29
                else:
                    lst[1] = 28
                for b in range(12):
                    st = ""
                    for c in range(lst[b]):
                        st += self.genDay("{}/{}/{} ".format(b+1, c + 1, start+a))
                    fd.write(st)

    def getPrice(self, food):
        if (food not in self.price):
            return (self.price["def"])
        return (self.price[food])

    def genDay(self, date):
        st = ""
        for a in range(self.daylen):
            if (random.randint(1, 100) < ONETIME_PROB):
                _id = -1
                c = self.genClient("oneTime")
            else:
                _id = random.randint(0, len(self.client) - 1)
                c = self.client[_id]
            probFood = random.randint(1, 100)
            probDrink = random.randint(1, 100)
            dr = self.probCorespond(probDrink, a, self.drink)
            fd = self.probCorespond(probFood, a, self.food)
            c.buy(dr, self.getPrice(dr))
            if (fd != ''):
                c.buy(fd, self.getPrice(fd))
            st += "{} {};{};{};{};\n".format(date, self.proba[a].getHour(), c.getId(), dr, fd)
            _type = c.getType()
            if (_id == 1 and c.canBuy(PRICE_MAX) == False):
                del self.client[_id]
        return (st)

def coffeeBar(start=13, years=5, out="gen.csv"):
    b = bar()
    b.genYears(start, years, out)

coffeeBar()
