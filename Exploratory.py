import csv

OUTPOUT_FILE = "stat.csv"
IN_FILE = 'Coffeebar_2013-2017.csv'

def getListTostr(lst):
    st = ""
    ln = len(lst)
    for a, b in enumerate(lst):
        st = st + b
        if (a < ln - 2):
            st += " , "
        elif (a < ln - 1):
            st += " and "
    return (st)

class bar():
    def __init__(self):
        self.customer = []
        self.regular = []
        self.time = {}
        self.food = ["sandwich", "pie", "muffin", "cookie", "nothing"]
        self.drink = [ "milkshake", "frappucino" , "soda" , "coffee" , "tea" , "water"]

    def addCommand(self, _command):
        tm =  _command[0].split(" ")
        if (len(tm) == 1):
            return
        if (_command[1] not in self.customer):
            self.customer.append(_command[1])
        elif (_command[1] not in self.regular):
            self.regular.append(_command[1])
        if (tm[1] not in self.time):
            self.time[tm[1]] = Time()
        self.time[tm[1]].addPlot(_command[2], "soda")
        if (_command[3] == ''):
            _command[3] = "nothing"
        self.time[tm[1]].addPlot(_command[3], "food")

    def getOwnTime(self):
        return (len(self.customer) - len(self.regular))

    def getBuyHour(self):
        a = list(self.time)
        for b in a:
            st = "On average the probability of a customer at {} {} is ".format(b, getListTostr(self.drink))
            for d, c in enumerate(self.drink):
                q = self.time[b].getFood(c)
                st += str(int(q / self.time[b].getType("soda") * 100)) + "%"
                if (d < len(self.drink) - 1):
                    st += ", "
            st += " and for food "
            for d, c in enumerate(self.food):
                st += str(int(self.time[b].getFood(c) / self.time[b].getType("food") * 100)) + "% {}".format(c)
                if (d < len(self.food) - 2):
                    st += ", "
                elif (d < len(self.food) - 1):
                    st += " and "
            print (st)

    def getStatInFile(self, name):
        with open(name, 'w') as fd:
            a = list(self.time)
            st = ""
            for b in a:
                st += b + ';'
                save = 0
                for d, c in enumerate(self.drink):
                    if (d == len(self.drink) - 1):
                        save = 100
                    else:
                        save += int(self.time[b].getFood(c) / self.time[b].getType("food") * 100)
                    st += "{} {}".format(c, save) + ";"
                save = 0
                for d, c in enumerate(self.food):
                    if (d == len(self.food) - 1):
                        save = 100
                    else:
                        save += int(self.time[b].getFood(c) / self.time[b].getType("food") * 100)
                    st += "{} ".format(c) + str(save) + ";"
                st += "\n"
            fd.write(st)

    def getTypeSell(self):
        a = list(self.time)
        dr = 0
        fd = 0
        for b in a:
            for c in self.drink:
                dr += self.time[b].getFood(c)
            for c in self.food:
                if (c != "nothing"):
                    fd += self.time[b].getFood(c)
        return (dr, fd)

class Time():
    def __init__(self):
        self.plot = {'soda' : 0, 'food' : 0}
        self.food = {}

    def addPlot(self, name, _type):
        try:
            self.food[name] += 1
        except KeyError:
            self.food[name] = 1
        self.plot[_type] += 1

    def getFood(self, name):
        try:
            return (self.food[name])
        except KeyError:
            return (0)

    def getType(self, _type):
        return (self.plot[_type])

def stat(_type="stat"):
    b = bar()
    with open(IN_FILE, 'r') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=';')
        for line in spamreader:
            b.addCommand(line)
        if (_type == "out"):
            b.getStatInFile(OUTPOUT_FILE)
        elif (_type == "stat"):
            b.getBuyHour()
        elif (_type == "diff"):
            print (b.getOwnTime())
        elif (_type=="quantity"):
            dr, fd = b.getTypeSell()
            print ("drink sell : {} , food sell : {}".format(dr, fd))

stat()
